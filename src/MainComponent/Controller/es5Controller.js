/**
 *
 *
 * @package
 * @licence MIT
 * @author Thomas Dupont
 */

const Response = require('decorator-module').response;

module.exports = {
    /**
     * @Route('/init', 'POST')
     * @Test('hello world')
     *
     * @param req
     */
    mainAction: function(req) { return new Response(true, 200, {action : 'init'}) }
};
