const CONF = require('../../../config/conf');
const Response = require('decorator-module').response;

/**
 * Main controller class
 */
class MainController {
	/**
	 * @Route('/info', 'POST')
     * @Test('hello world')
	 * @CheckInCall('test')
	 *
     * @param req
     * @returns {Response}
     */
	static mainAction(req) {
        /**
		 *
         */
		return new Response(true, 200, { message: 'api ok' });
	}

	/**
	 *
     * @param req
     * @returns {Response}
     */
	static testAction(req) {
		return new Response(true, 200, { message: 'test action' });
	}

    /**
	 * @Route('/customer', 'POST')
	 *
	 * @BodyParam('id', '^([0-9a-f]*)$')
     * @param req
     */
	static getInfoAction(req) {
        return new Response(true, 200, { result: {
        	lastname: 'Dupont',
			firstname: 'Thomas'
		}});
	}
}

module.exports = MainController;

